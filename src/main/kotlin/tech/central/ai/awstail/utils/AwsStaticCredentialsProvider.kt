package tech.central.ai.awstail.utils

import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.AWSCredentialsProvider

class AwsStaticCredentialsProvider(private val credentialsInput: AWSCredentials?) : AWSCredentialsProvider {
    override fun getCredentials(): AWSCredentials? = credentialsInput
    override fun refresh() {}
}
