package tech.central.ai.awstail.writers

import com.amazonaws.services.logs.model.FilteredLogEvent
import com.amazonaws.services.logs.model.ResourceNotFoundException
import tech.central.ai.awstail.AWSLogsClient

abstract class LogWriter(val awsLogs: AWSLogsClient) {

    fun write() {
        try {
            writeLogsIntroduction()
            if (awsLogs.arguments.follow) tailLogs() else fetchLogs()
        } catch (e: ResourceNotFoundException) {
            if (!awsLogs.arguments.prefix.isNullOrEmpty() || !awsLogs.arguments.suffix.isNullOrEmpty()) {
                tryWithoutPreAndSuffix()
            } else {
                writeFailMessage("Error: We could not find the requested log!")
            }
        }
    }

    fun fetchLogs() {
        awsLogs.processLogEvents { writeEvent(it) }
    }

    private fun tryWithoutPreAndSuffix() {
        writeFailMessage("Error: We could not find the requested log. We are trying again without prefix and suffix!")
        awsLogs.arguments.prefix = ""
        awsLogs.arguments.suffix = ""
        write()
    }

    abstract fun tailLogs()

    protected abstract fun writeLogsIntroduction()

    protected abstract fun writeFailMessage(failMessage: String)

    protected abstract fun writeEvent(event: FilteredLogEvent)

}