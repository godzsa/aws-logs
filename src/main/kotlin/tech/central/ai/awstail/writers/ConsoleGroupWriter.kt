package tech.central.ai.awstail.writers

import com.amazonaws.services.logs.model.LogGroup
import tech.central.ai.awstail.AWSLogsClient
import tech.central.ai.awstail.utils.TerminalColor

class ConsoleGroupWriter(awsLogs: AWSLogsClient) : GroupWriter(awsLogs) {

    override fun writeGroupNameIntroduction() = println("Fetching log group names..." colored TerminalColor.COLOR_GREEN)

    override fun writeGroupName(logGroup: LogGroup) = println(logGroup.logGroupName)

    private infix fun String.colored(color: TerminalColor): String = "${color.escapeSequence}$this${TerminalColor.COLOR_RESET.escapeSequence}"

}