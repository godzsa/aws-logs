package tech.central.ai.awstail.writers

import com.amazonaws.services.logs.model.LogGroup
import tech.central.ai.awstail.AWSLogsClient

abstract class GroupWriter(val awsLogs: AWSLogsClient) {

    fun write() {
        writeGroupNameIntroduction()
        awsLogs.processGroupNames { writeGroupName(it) }
    }

    protected abstract fun writeGroupNameIntroduction()

    protected abstract fun writeGroupName(logGroup: LogGroup)

}