package tech.central.ai.awstail

import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.AWSCredentialsProvider
import com.amazonaws.auth.AWSCredentialsProviderChain
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain
import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.regions.AwsProfileRegionProvider
import com.amazonaws.regions.AwsRegionProvider
import com.amazonaws.regions.AwsRegionProviderChain
import com.amazonaws.regions.DefaultAwsRegionProviderChain
import com.amazonaws.services.logs.AWSLogs
import com.amazonaws.services.logs.AWSLogsClientBuilder
import com.amazonaws.services.logs.model.*
import tech.central.ai.awstail.utils.AwsStaticCredentialsProvider
import tech.central.ai.awstail.utils.AwsStaticRegionProvider

class AWSLogsClient(var arguments: AWSTailArguments, private val credentials: AWSCredentials? = null, private val region: String? = null) {
    var nextToken: String? = null
    var lastUpdate = arguments.startTime

    /**
     * Try:
     * 1. credentials provided
     * 2. profile in .aws/credentials, if --profile is defined
     * 3. default chain:
     *    1. Env variables
     *    2. System variables
     *    3. Default credentials profile
     *    4. EC2 container credentials
     */
    private fun credentialsProvider(): AWSCredentialsProvider = AWSCredentialsProviderChain(
            AwsStaticCredentialsProvider(credentials),
            ProfileCredentialsProvider(arguments.profile),
            DefaultAWSCredentialsProviderChain())

    /**
     * For this to work you must either specify a region name, or setup a profile with a .aws/config file, region set
     * If assuming role make sure you add the region in the config file under the name of the assumed role
     */
    private fun regionProvider(): AwsRegionProvider = AwsRegionProviderChain(AwsStaticRegionProvider(region), AwsProfileRegionProvider(arguments.profile), DefaultAwsRegionProviderChain())

    val awsLogs: AWSLogs = AWSLogsClientBuilder.standard()
            .withCredentials(credentialsProvider())
            .withRegion(regionProvider().region)
            .build()

    fun processGroupNames(action: (LogGroup) -> Unit) {
        do {
            val result = awsLogs.describeLogGroups(DescribeLogGroupsRequest().withNextToken(nextToken))
            result.logGroups.forEach { action(it) }
            nextToken = result.nextToken
        } while (!result.nextToken.isNullOrEmpty())
    }

    fun processLogEvents(action: (FilteredLogEvent) -> Unit) {
        do {
            val filterLogEvents = awsLogs.filterLogEvents(filterRequest())
            filterLogEvents.events.forEach { action(it) }
            update(filterLogEvents)
        } while (!filterLogEvents.nextToken.isNullOrEmpty())
    }

    private fun filterRequest(): FilterLogEventsRequest = FilterLogEventsRequest()
            .withLogGroupName(arguments.logGroupName)
            .withLimit(arguments.n)
            .withStartTime(lastUpdate!! + 1) //TODO refactor maybe?
            .withEndTime(arguments.endTime)
            .withNextToken(nextToken)
            .withInterleaved(true)


    private fun update(filterLogEvents: FilterLogEventsResult) {
        val events = filterLogEvents.events

        if (events.isNotEmpty()) {
            lastUpdate = events.last().timestamp
        }
        nextToken = filterLogEvents.nextToken
    }
}